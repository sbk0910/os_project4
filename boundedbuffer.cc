#include "boundedbuffer.h"

BoundedBuffer::BoundedBuffer(int maxsize)
{
     close = false;
     lock = new Lock("BufferLock");
     BufferEmpty = new Condition("BufferEmpty");
     BufferFull = new Condition("BufferFull");
     Buffer = new char[maxsize];
     BufferSize = maxsize;
     number = 0;
}

BoundedBuffer::~BoundedBuffer()
{
      delete Buffer;
      delete lock;
      delete BufferEmpty;
      delete BufferFull;
}

int BoundedBuffer::Read(char *data, int size)
{
    if(close){
      return 0;
    }
     lock->Acquire();
     cout <<"READ " << size <<"size data" <<endl;
     for ( int i=0,j=0; i<BufferSize && j<size; i++,j++ )
     {
         if ( number == 0 )
         {
              BufferEmpty->Wait(lock);
              i = 0;
         }
         else
              {
									 data[i] = Buffer[j];
                   cout << "\t\t\t read " << j+1 << "`s data : " << data[j] <<endl;
                   number--;

                   if ( number == BufferSize-1 )
                        BufferFull->Signal(lock);
              }
     }
     lock->Release();
     return size;
}

int BoundedBuffer::Write(char *data, int size)
{
    if(close){
      return 0;
    }
     lock->Acquire();
     for ( int i=0,j=0; i<BufferSize && j<size; i++,j++ )
     {
         if ( number == BufferSize )
         {
              BufferFull->Wait(lock);
              i = 0;
         }
         else
              {
                   Buffer[i] = data[j];
                   number++;
                   cout <<"write " << number <<" `s data : " << Buffer[i] << endl;
                   if ( number == 1 )
                        BufferEmpty->Signal(lock);
              }
     }
     lock->Release();
     return size;
}

void BoundedBuffer::Close(){
  delete Buffer;
  Buffer = NULL;
  close = true;
}
