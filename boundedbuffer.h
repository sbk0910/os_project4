#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H

#include "synch.h"

class BoundedBuffer
{
         public:
                // create a bounded buffer with a limit of 'maxsize' bytes
                BoundedBuffer(int maxsize);
                ~BoundedBuffer(); //Îö¹¹º¯Êý

                // read 'size' bytes from the bounded buffer, storing into 'data'.
                // ('size' may be greater than 'maxsize')
                int Read(char *data, int size);

                // write 'size' bytes from 'data' into the bounded buffer.
                // ('size' may be greater than 'maxsize')
                int Write(char *data, int size);
                void Close();

         private:
                 char *Buffer;
                 int BufferSize;
                 int number;
                 bool close;
                 Lock *lock;
                 Condition *BufferEmpty;
                 Condition *BufferFull;
};

void BufferRead(int dataN);
void BufferWrite(int dataN);
extern void BufferTest();

#endif
