#include <iostream>
#include "boundedbuffer.h"
#include <string>


using namespace std;

BoundedBuffer *buffer;

void BufferWrite(int dataN){
  char a[10] = {0};
  a[0] = 'A';
  for(int i = 1; i < dataN; i++){
    a[i] = ++a[i-1];
  }
  int size = buffer->Write(a, dataN);
  cout << "Write char : " << a << " int buffer " <<endl;
  cout << "return size : " << size << endl;
  cout << "----------------------------------" <<endl;
}

void BufferRead(int dataN){
  char a[10] = {0};
  int size = buffer->Read(a, dataN);
  cout << "Read char : " << a << " int buffer " <<endl;
  cout << "return size : " << size << endl;
  cout << "----------------------------------" <<endl;
}

void BufferTest(){

  buffer = new BoundedBuffer(5);
  int dataN = 5;
  cout << "hello buffer" <<endl;

  BufferWrite(5);
  BufferRead(5);

  buffer->Close();
  BufferWrite(1);
/*
  Thread *t1 = new Thread("forked thread");
  Thread *t2 = new Thread("forked thread");
  t1->Fork((VoidFunctionPtr)BufferWrite, (void*)dataN);
  t2->Fork((VoidFunctionPtr)BufferRead, (void*)dataN);*/

}
